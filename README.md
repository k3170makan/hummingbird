# Project Humming Bird

A python framework for controlling a DJI Tello Drone

Project Humming Bird is a stand a lone python framework that provides
a convenient wrapper around DJI's UDP based command API for the Tello Drone.


## Usage
Before anything please ensure your DJI Tello Drone is turned on, has enough battery power (preferably more than 10) and most importantly that your machine is connected to the drone's wifi.

```
>$ ./main.py --help
usage: main.py [-h] [-v]

Project HummingBird : Controlling a Tello Drone

optional arguments:
  -h, --help       show this help message and exit
  -v, --cvcapture  Use cvVideoCapture instead of direct udp streaming. Startup will be much slower!

```

## Commands

The following list of commands are currently supported, to use them simply launch humming bird by executing
main and when the connections are setup just type them into the terminal.

* takeoff -- command the drone to take off
* land -- land the drone as the next action
* emergency -- emergency landing
* streamon -- turn the video stream on, behaviour will depend on the cv_capture switch
* streamoff -- turn the stream off
* keyboard -- turn into keyboard mode to control the drone in a joy-stick like manner
* curious -- use the curious bird flight pattern
* battery -- display battery level
* time -- show flight time
* speed -- show speed
* follow -- follow any face detected by the video stream
* unfollow -- disable follow mode

## Features
Current Features include:
* Live Feed with Joystick-like control from the keyboard
* A command shell with some helpful commands like: battery, speed,
* Face detection during display (face tracking comming soon!)

## Examples

```
>$ ./main.py 
* initialzing connections...
* starting cmd recv thread... 
* entering command mode...
>> sent [7] bytes
humming_bird>takeoff
* taking off...
>> sent [7] bytes
humming_bird>streamon
* turning stream on...
* starting video recv thread... 
```
