#!/bin/bash

echo "[*] connecting to drone..."
SSID=`sudo iw dev wlp1s0 scan | grep SSID | grep TELLO-61A2EF | awk -F\  '{ print $2 }'`
if [ "$SSID" != "" ] 
then
	echo "[*] drone is up @<"$SSID">"
	nmcli --ask dev wifi  connect $SSID
else
	echo "[x] cannot find Tello AP"
fi
