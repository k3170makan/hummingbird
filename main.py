#!/usr/bin/python3
import argparse
import cv2
import threading

from command_shell.command_shell import CommandShell
from connection import drone_connection
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Project HummingBird : Framework for controlling a DJI Tello Drone")
    parser.add_argument('--cvcapture',default=False,action='store_true',help="Use cvVideoCapture instead of direct udp streaming. Startup will be much slower!")
    parser.add_argument('-v','--verbose',default=False,type=bool,help="verbose output including debugg messages")
    parser.add_argument('-f','--video_output',default="./hummingbird_video_output.mp4",type=str,help="file path to save video output too")
    parser.add_argument('-H','--haarfile',default="./haarcascade_frontalface_default.xml",type=str,help="path to haar classifier cascade file")

    args = parser.parse_args()
    CASCADE_PATH=args.haarfile
    cascade_classifier = cv2.CascadeClassifier(CASCADE_PATH)
    #action_thread = threading.Thread(target=drone_connection.action_loop,args=(hm,))
    cs = CommandShell()
    cs.command_loop()
