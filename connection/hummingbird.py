#!/usr/bin/python3
from . import drone_connection

class HummingBird(drone_connection.DroneConnection):
    def __init__(self,flight_pattern = None,cv_capture=False):
            super().__init__(cv_capture=cv_capture)            
            self.pattern = flight_pattern
            self.in_command = False
            self.init()

            return
    def command_mode(self):
        print("* entering command mode...")
        self.in_command = True
        self.con_send("command")
    def command_loop(self):
        return
    def takeoff(self):
        print("* taking off...")
        if not(self.in_command):
            self.command_mode()
        self.con_send("takeoff")
        return

    def emergency(self):
        print("* initiating emergency mode...")
        self.con_send("emergency")
        self.con_recv()
        return

    def land(self):
        print("* landing...")
        if not(self.in_command):
            self.command_mode()
        self.con_send("land")
        return

    def stream_on(self,cv_capture=False):
        print("* turning stream on...")
        self.con_send("streamon")

        if cv_capture:
            self.cvcapture_thread.start()
        self.vstream_recv_thread.start()
        self.render_thread.start()
        if not(self.in_command):
            self.command_mode()

    def stream_off(self,cv_capture=False):
        print("* turning stream off...")
        if not(self.in_command):
            self.command_mode()
        self.con_send("streamoff")

        if cv_capture:
            self.cvcapture_thread.join()
        else:
            self.vstream_recv_thread.join()

        self.render_thread.join()

    def cw(self,x=30):
        if not(self.in_command):
            self.command_mode()
        self.con_send("cw %d" % (x))

    def ccw(self,x=30):
        if not(self.in_command):
            self.command_mode()
        self.con_send("ccw %d" % (x))

    def forward(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("forward %d" % (x))

    def back(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("back %d" % (x))


    def up(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("up %d" % (x))


    def down(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("down %d" % (x))

    def left(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("left %d" % (x))

    def right(self,x=20):
        if not(self.in_command):
            self.command_mode()
        self.con_send("right %d" % (x))

    def battery(self):
        if not(self.in_command):
            self.command_mode()
        return self.con_send_ping("battery?")

    def time(self):
        if not (self.in_command):
            self.command_mode()
        return self.con_send_ping("time?")

    def speed(self):
        if not (self.in_command):
            self.command_mode()
        return self.con_send_ping("speed?")

    def execute_pattern(self,pattern=None):
        print("* executing pattern ")
        pattern = pattern
        if pattern == None:
            pattern = self.pattern
        pattern.set(self)
        pattern.loop()
