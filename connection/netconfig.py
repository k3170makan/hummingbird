#!/usr/bin/python3

DRONE_IP = "192.168.10.1"
LOCAL_IP = ""

COMMAND_DPORT = 8889 #send commands to this port
COMMAND_SPORT = 9999 #send commands to this port
SCALE=2
FRAME_WIDTH = 960
FRAME_HEIGHT = 720
FRAME_CENTER = (int(FRAME_WIDTH/2),int(FRAME_HEIGHT/2))
VSTREAM_SPORT = 11111 #read video farmes from this port
STATE_DPORT = 8890 #read state from this port
