#!/usr/bin/python3
import socket
import sys
import random
import time
import threading
import h264decoder
import numpy as np
import queue
import cv2
from .netconfig import *

CURRENT_FRAME = None
FRAME_QUEUE = queue.Queue()
COMMAND_RESP_QUEUE = queue.Queue()
ACTION_QUEUE = queue.Queue()

decoder = h264decoder.H264Decoder()
CASCADE_PATH = "classifier/haarcascade_frontalface_default.xml"
cascade_classifier = cv2.CascadeClassifier(CASCADE_PATH)


class DroneConnection:
    def __init__(self, drone_ip = DRONE_IP,
        command_port = COMMAND_DPORT,
        vstream_port = VSTREAM_SPORT,
        state_port = STATE_DPORT,cv_capture=False):
        self.cv_capture = cv_capture
        self.drone_ip = drone_ip
        self.command_port = command_port
        self.vstream_port = vstream_port
        self.state_port = state_port

        self.command_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.state_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.vstream_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.initd = False

    def init(self):
        try:

            self.vstream_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.command_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ret = self.vstream_socket.bind((LOCAL_IP,int(self.vstream_port)))
            ret = self.command_socket.bind((LOCAL_IP,int(self.command_port))) #bind local socket

            if self.cv_capture:
                self.video_capture = cv2.VideoCapture("udp://@%s:%s" % (LOCAL_IP,self.vstream_port))
                self.cvcapture_thread = threading.Thread(target=self.cvcapture_loop)
            self.vstream_recv_thread = threading.Thread(target=self.vid_recv_loop)
            self.recv_thread = threading.Thread(target=self.con_recv_loop)
            self.render_thread = threading.Thread(target=self.render_loop)

            self.recv_thread.start()

        except Exception as e:
            print("[%s] %s" % ("drone_connection.init",e))
            pass

    def vid_recv_loop(self):
        print("* starting video recv thread... ")
        video_data = bytes()
        sock_data = bytes()
        while (True):
            #try:
            sock_data = self.vstream_socket.recvfrom(2048)[0]
            for framedata in decoder.decode(sock_data):
               frame = self.convert_frame(framedata)
               faces,frame = self.detect_faces(frame)
               #if len(faces) > 0:
               #    detect_actions(frame,faces[0])
               FRAME_QUEUE.put(frame)
                   
            #except Exception as err:
            #    print("[%s] %s" % ("vid_recv_loop",str(err)))
            #    pass 
        return video_data
    
    def cvcapture_loop(self):
        ret, frame = self.video_capture.read()
        if ret:
            faces,frame = self.detect_faces(frame)
            if len(faces) > 0:
                self.detect_actions(frame,faces[0])
            FRAME_QUEUE.put(frame)
    
    def convert_frame(self,framedata):
        (frame,w,h,ls) = framedata
        frame = np.frombuffer(frame, dtype=np.ubyte, count=len(frame))
        frame = (frame.reshape((h, ls // 3, 3)))
        frame = frame[:, :w, :]
        return frame
    
    def detect_actions(self,frame,face):
        (x,y,w,h) = face
        face_center = (int(x+w/2),int(y+h/2))
        delta = (FRAME_CENTER[0]-face_center[0],FRAME_CENTER[1]-face_center[1])
        if delta[0] > 0 and delta[1] > 0:
            ACTION_QUEUE.put("right")
            ACTION_QUEUE.put("up")
        if delta[0] > 0 and delta[1] < 0:
            ACTION_QUEUE.put("right")
            ACTION_QUEUE.put("down")
        if delta[0] < 0 and delta[1] > 0:
            ACTION_QUEUE.put("left")
            ACTION_QUEUE.put("up")
        if delta[0] < 0 and delta[1] < 0:
            ACTION_QUEUE.put("left")
            ACTION_QUEUE.put("down")
    
    def preprocess_frame(self,frame):
        new_w,new_h = int(FRAME_WIDTH/SCALE), int(FRAME_HEIGHT/SCALE)
        new_center_w,new_center_h = new_w/2,new_h/2
        #print(list(map(type,[new_w,new_h,new_center_w,new_center_h])))
    
        return (new_center_w,new_center_h),cv2.resize(frame,(new_w,new_h))
    
    def detect_faces(self,frame):
        new_center = ()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        new_center,frame = self.preprocess_frame(frame)
        gray  = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = cascade_classifier.detectMultiScale(gray,
                                          scaleFactor=1.1,
                                          minNeighbors=5,
                                          minSize=(30,30),
                                          flags = cv2.CASCADE_SCALE_IMAGE)
        if len(faces) != 0:
            (x,y,w,h) = faces[0]
            
            cv2.rectangle(frame,(int(x),int(y)),(int(x+w),int(y+h)),(0,250,0),1)
            new_center = (int(new_center[0]),int(new_center[1]))
            cv2.circle(frame,new_center,5,(250,0,0),1) 
            face_center = (int(x+w/2),int(y+h/2))
            cv2.circle(frame,face_center,5,(0,250,0),1) 
            delta = (FRAME_CENTER[0]-face_center[0],FRAME_CENTER[1]-face_center[1])
            cv2.line(frame, FRAME_CENTER,face_center,(0,0,250),1)
    
        return faces,frame
    
    def render_loop(self):
        while (True):
            if not(FRAME_QUEUE.empty()):
                cv2.imshow("Tello",FRAME_QUEUE.get())
                if cv2.waitKey(1) & 0xFF == ord('Q'):
                    cv2.destroyAllWindows()
    
    def action_loop(self,drone=None):
         while (True):
            if not(ACTION_QUEUE.empty()):
                action = ACTION_QUEUE.get()
                if action == "up":
                    print("action: up...")
                    drone.up()
                if action == "down":
                    print("action: down...")
                    drone.down()
                if action == "left":
                    print("action: left...")
                    drone.left()
                if action == "right":
                    print("action: right...")
                    drone.right()
                time.sleep(2) 


    def con_recv_loop(self):
        while (True):
            data, server = self.command_socket.recvfrom(64)
            print("<< %s\n" % (data))
            # set the state or respond to it here

    def con_recv(self):
        data = self.command_socket.recvfrom(64)
        print("<<", data)
        return data

    def con_send(self,data="",ip=DRONE_IP,port=COMMAND_DPORT):
        #print("[*] ip:%s<%s>, port:%s<%s> [%s] %s" % (ip,type(ip),port,type(ip),data,self.command_socket))
        ret = 0
        try:
            ret = self.command_socket.sendto(bytes(data.encode(encoding="utf8")), (ip,int(port)))
            #ret = self.command_socket.send(bytes(data.encode()))
            print(">> return code: %s" % (ret))
        except OSError as e:
            sys.stderr.write("[con_send] %s" % (e))
            pass
        if ret == len(data):
            print(">> sent [%d] bytes" % (ret))
        return

    def con_send_ping(self,data="",ip=DRONE_IP,port=COMMAND_DPORT):
        #print("[*] ip:[%s], port:[%s]" % (ip,port))
        ret = 0
        try:
            ret = self.command_socket.sendto(bytes(data.encode(encoding="utf8")), (ip,int(port)))
        except OSError as e:
            sys.stderr.write("[con_send_ping] %s" % (e))
            #self.init()
            #ret = self.command_socket.sendto(bytes(data.encode()), (ip,int(port)))
            ret = self.command_socket.send(bytes(data.encode()))
            pass

        if ret == len(data):
            return data
        return -1
