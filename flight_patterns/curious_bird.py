#!/usr/bin/python3
from flight_pattern import FlightPattern

class CuriousBird(FlightPattern):
    def __init__(self):
        super(FlightPattern).__init__()
    def look_left(self):
        angle = int(random.random()*9999%270 + 10)
        self.connection.con_send("ccw %d" % (angle))
        time.sleep(2)
        self.connection.con_send("cw %d" % (angle))
        time.sleep(2)

    def look_right(self):
        angle = int(random.random() * 9999 % 180 + 10)
        self.connection.con_send("cw %d" % (angle))
        time.sleep(2)
        self.connection.con_send("ccw %d" % (angle))
        time.sleep(2)

    def loop(self):
        #make the drone loook curious
        if (int(random.random()*999%10) > 5):
            self.look_left()
        else:
            self.look_right()

