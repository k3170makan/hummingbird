import sys
from pathlib import Path

file = Path(__file__).resolve()
parent,top = file.parent,file.parents[3]

sys.path.append(str(top))
try:
    sys.path.remove(str(parent))
except ValueError:
    pass
