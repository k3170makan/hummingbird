#!/usr/bin/python3
import sys
from pathlib import Path

file = Path(__file__).resolve()
parent,top = file.parent,file.parents[3]

sys.path.append(str(top))
try:
    sys.path.remove(str(parent))
except ValueError:
    pass

from connection import hummingbird

class CommandShell:
    def __init__(self,prompt="hummingbird>",cv_capture=False,con=hummingbird.HummingBird()):
        self.cv_capture = cv_capture
        self.connection = con
        self.prompt = prompt

    def command_loop(self):
        while True:
            msg = ""
            if not(self.cv_capture):
                print("humming_bird>",end='')
                msg = input("")
            else:
                msg = input("humming_bird> ")
            if msg == "land":
                self.connection.land()
            if msg == "emergency":
                self.connection.emergency()
            if msg == "takeoff":
                self.connection.takeoff()
            if msg == "battery":
                self.connection.battery()
            if msg == "time":
                self.connection.time()
            if msg == "speed":
                self.connection.speed()
            if msg == "streamon":
                self.connection.stream_on(cv_capture=self.cv_capture)
            if msg == "streamoff":
                self.connection.stream_off(cv_capture=self.cv_capture)
            if msg == "keyboard":
                print("* WARNING: entering keyboard mode...")
                while True:
                    try:
                        key = cv2.waitKey(10)
                        if key == ord(" "):
                            self.connection.forward(30)
                        if key == ord("w"):
                            self.connection.up(30)
                        if key == ord("S"):
                            self.connection.down(30)
                        if key == ord("s"):
                            self.connection.back(30)
                        if key == ord("d"):
                            self.connection.right(30)
                        if key == ord("a"):
                            self.connection.left(30)
                        if key == ord("x"):
                            self.connection.land(30)
                        if key == ord("t"):
                            self.connection.takeoff()
                        if key == ord("q"):
                            self.connection.ccw(45)
                        if key == ord("e"):
                            self.connection.cw(45)

                    except KeyboardInterrupt as k:
                        print("* leaving keyboard mode..")
                        break
            if msg == "follow":
                print("* starting follow mode...")
                action_thread.start()
            if msg == "unfollow":
                print("* stopping follow mode...")
                action_thread.join()
            if msg == "curious":
                for i in range(5):
                    try:
                        self.connection.execute_pattern()
                        time.sleep(1)
                    except KeyboardInterrupt:
                        break


